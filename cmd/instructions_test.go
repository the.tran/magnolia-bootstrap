package cmd

import (
	"bytes"
	"fmt"
	"io"
	"testing"
)

func TestParseInstructions(t *testing.T) {
	type wantFunc func(in *InstructionList) error

	file1 := bytes.NewBufferString(fixture("endpoints.yml", t))

	tests := []struct {
		name    string
		in      io.Reader
		want    wantFunc
		wantErr bool
	}{
		{
			name: "endpoints",
			in:   file1,
			want: func(in *InstructionList) error {
				for k, v := range in.Playbook {
					fmt.Println(k, v)
				}
				return nil
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := new(InstructionList)
			if err := getInstructionList(v, tt.in); (err != nil) != tt.wantErr {
				t.Errorf("wanterr = false, but err = %v", err)
			}
			if tt.want != nil {
				if err := tt.want(v); err != nil {
					t.Error(err)
				}
			}
		})
	}
}
