package cmd

import (
	"bytes"
	"context"
	"encoding/json"
	"mgnlboot/cmd/magnolia"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"gopkg.in/yaml.v3"
)

// Let's test our executor a bit.
func TestDo(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/.rest/nodes/v1/testing/nolongerneeded" && r.Method == http.MethodDelete {
			return
		}

		if r.URL.Path == "/.rest/nodes/v1/testing/nolongerneeded" && r.Method == http.MethodGet {
			node := magnolia.NewNode("test")
			if err := json.NewEncoder(w).Encode(&node); err != nil {
				t.Fatal(err)
			}
			return
		}

		if r.URL.Path == "/.rest/nodes/v1/testing/puttest/schni" && r.Method == http.MethodGet {
			node := magnolia.NewNode("test")
			if err := json.NewEncoder(w).Encode(&node); err != nil {
				t.Fatal(err)
			}
			return
		}
		if r.URL.Path == "/.rest/nodes/v1/testing/puttest/schni" && r.Method == http.MethodPost {
			return
		}
		if r.URL.Path == "/.rest/nodes/v1/testing/puttest" && r.Method == http.MethodPut {
			t.Errorf("puttest: got = %s, want = %s", r.Method, http.MethodPost)
		}

		if r.URL.Path == "/.rest/nodes/v1/testing/normalput/schni" && r.Method == http.MethodGet {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		if r.URL.Path == "/.rest/nodes/v1/testing/normalput" && r.Method == http.MethodPut {
			return
		}
		if r.URL.Path == "/.rest/nodes/v1/testing/normalput" && r.Method == http.MethodPost {
			t.Errorf("normalput: got = %s, want = %s", r.Method, http.MethodPost)
		}

		if strings.HasPrefix(r.URL.Path, "/.rest/nodes/v1/testing/putauth") {
			header := r.Header.Get("Authorization")
			wantHeader := "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.POstGetfAytaZS82wHcjoTyoqhMyxXiWdR7Nn7A29DNSl0EiXLdwJ6xC6AfgZWF1bOsS_TuYI3OG85AmiExREkrS6tDfTQ2B3WXlrr-wp5AokiRbz3_oB4OxG-W9KcEEbDRcZc0nH3L7LzYptiy1PtAylQGxHTWZXtGz4ht0bAecBgmpdgXMguEIcoqPJ1n3pIWk_dUZegpqx0Lka21H6XxUTxiy8OcaarA8zdnPUnV6AmNP3ecFawIFYdvJB_cm-GvpCSbr8G8y_Mllj8f4x9nBH8pQux89_6gUY618iYv7tuPWBFfEbLxtF2pZS6YC1aSfLQxeNe8djT9YjpvRZA"
			if header == wantHeader {
				switch r.Method {
				// We make sure the GET request which comes before the PUT request is also authenticated.
				case http.MethodGet:
					http.Error(w, "", http.StatusNotFound)
					return
				case http.MethodPut:
					node := magnolia.NewNode("test")
					if err := json.NewEncoder(w).Encode(&node); err != nil {
						t.Fatal(err)
					}
					return
				default:
					http.Error(w, "", http.StatusMethodNotAllowed)
					return
				}
			}
			http.Error(w, "auth header incorrect", http.StatusUnauthorized)
			t.Errorf("authget: got = %s, want = %s", header, wantHeader)
			return
		}

		t.Errorf("this should not happen: path: %s", r.URL.Path)
		w.WriteHeader(http.StatusBadRequest)
	}))
	defer ts.Close()

	buf := bytes.NewBufferString(fixture("deleteInstruction.yml", t))

	var deleteInst Instruction
	if err := yaml.NewDecoder(buf).Decode(&deleteInst); err != nil {
		t.Fatalf("could not decode yaml from fixture: %v", err)
	}

	inst2 := deleteInst
	inst2.Method = http.MethodOptions // Let's change the method to something invalid.

	buf2 := bytes.NewBufferString(fixture("singleDataInstruction.yml", t))
	var puttest Instruction
	if err := yaml.NewDecoder(buf2).Decode(&puttest); err != nil {
		t.Fatalf("could not decode yaml from fixture: %v", err)
	}

	puttest.Path = "/.rest/nodes/v1/testing/puttest"
	puttest.Method = "put"

	normalput := puttest
	normalput.Path = "/.rest/nodes/v1/testing/normalput"
	normalput.Method = "put"

	buf3 := bytes.NewBufferString(fixture("authedInstruction.yml", t))
	var putauth Instruction
	if err := yaml.NewDecoder(buf3).Decode(&putauth); err != nil {
		t.Fatalf("could not decode yaml from fixture: %v", err)
	}

	client, err := magnolia.NewClient(
		magnolia.WithBaseURL(ts.URL),
	)
	if err != nil {
		t.Fatal(err)
	}

	executor := NewExecutor(client, nil, false)

	type args struct {
		ctx context.Context
		ins *Instruction
	}
	tests := []struct {
		name    string
		e       *Executor
		args    args
		wantErr bool
	}{
		{
			name: "simple delete request",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &deleteInst,
			},
			wantErr: false,
		},
		{
			name: "invalid method",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &inst2,
			},
			wantErr: true,
		},
		{
			name: "put test, should be post",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &puttest,
			},
			wantErr: false,
		},
		{
			name: "put test, should stay put",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &normalput,
			},
			wantErr: false,
		},
		{
			name: "put test, with auth header",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &putauth,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.e.Do(tt.args.ctx, tt.args.ins); (err != nil) != tt.wantErr {
				t.Errorf("Do() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Bug #2
func TestDeleteIdempotency(t *testing.T) {
	buf := bytes.NewBufferString(fixture("deleteInstruction.yml", t))
	var deleteInst Instruction
	if err := yaml.NewDecoder(buf).Decode(&deleteInst); err != nil {
		t.Fatalf("could not decode yaml from fixture: %v", err)
	}
	deleteInst2 := deleteInst
	deleteInst2.Path = "testing/alreadydeleted"

	r := mux.NewRouter()
	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// We do not want to return 404 because that wouldn't be an errro for
		// the delete operation.
		t.Error("wrong url", r.URL, r.Method)
		http.Error(w, "wrong url", http.StatusInternalServerError)
	})
	var n int
	r.HandleFunc("/.rest/nodes/v1/testing/nolongerneeded", func(w http.ResponseWriter, r *http.Request) {
		n++
		node := magnolia.NewNode("test")
		if err := json.NewEncoder(w).Encode(&node); err != nil {
			t.Fatal(err)
		}
		t.Logf("%s request #%d, path = %s", r.Method, n, r.RequestURI)
	})
	var k int
	r.HandleFunc("/.rest/nodes/v1/testing/alreadydeleted", func(w http.ResponseWriter, r *http.Request) {
		k++
		t.Logf("%s request #%d, path = %s", r.Method, k, r.RequestURI)
		http.Error(w, "", http.StatusNotFound)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()
	client, err := magnolia.NewClient(
		magnolia.WithBaseURL(ts.URL),
	)
	if err != nil {
		t.Fatal(err)
	}
	executor := NewExecutor(client, nil, false)

	type args struct {
		ctx context.Context
		ins *Instruction
	}
	tests := []struct {
		name    string
		e       *Executor
		args    args
		wantErr bool
	}{
		{
			name: "delete existing node",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &deleteInst,
			},
			wantErr: false,
		},
		{
			name: "delete non-existing node",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &deleteInst2,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.e.Do(tt.args.ctx, tt.args.ins); (err != nil) != tt.wantErr {
				t.Errorf("Do() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Bug #3
func TestChangeHost(t *testing.T) {
	r := mux.NewRouter()
	r.HandleFunc("/author/.rest/configuration/test/authschni", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "", http.StatusNotFound)
	}).Methods(http.MethodGet)
	r.HandleFunc("/author/.rest/configuration/test", func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "", http.StatusOK)
	}).Methods(http.MethodPut)

	r.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// We do not want to return 404 because that wouldn't be an error for
		// the delete operation.
		t.Error("wrong url", r.URL)
		http.Error(w, "wrong url", http.StatusInternalServerError)
	})
	ts := httptest.NewServer(r)
	defer ts.Close()

	client, err := magnolia.NewClient(
		magnolia.WithEndpoint("configuration"),
		magnolia.WithBaseURL(ts.URL),
	)
	if err != nil {
		t.Fatal(err)
	}
	executor := NewExecutor(client, nil, false)

	buf := bytes.NewBufferString(fixture("hostChangedInstruction.yml", t))
	var hostChangeInst Instruction
	if err := yaml.NewDecoder(buf).Decode(&hostChangeInst); err != nil {
		t.Fatalf("could not decode yaml from fixture: %v", err)
	}

	// Now change the host.
	hostChangeInst.Host = ts.URL + "/author"

	type args struct {
		ctx context.Context
		ins *Instruction
	}
	tests := []struct {
		name    string
		e       *Executor
		args    args
		wantErr bool
	}{
		{
			name: "test host change",
			e:    executor,
			args: args{
				ctx: context.TODO(),
				ins: &hostChangeInst,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.e.Do(tt.args.ctx, tt.args.ins); (err != nil) != tt.wantErr {
				t.Errorf("Do() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
