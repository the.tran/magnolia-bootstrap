package cmd

import (
	"bytes"
	"context"
	"fmt"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func templateString(str string) (string, error) {
	var out bytes.Buffer
	in := bytes.NewBufferString(str)
	if err := templateThis(&out, in); err != nil {
		return "", err
	}
	return out.String(), nil
}

var waitForCmd = &cobra.Command{
	Use:   "waitfor",
	Short: "Wait for hostnames to resolve in DNS.",
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["waitfor"]
		_ = setVerbosity(vip.GetInt("verbosity"))
		logw := logrus.StandardLogger().WriterLevel(logrus.TraceLevel)
		defer logw.Close()
		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		flagNames := vip.GetString("names")
		if flagNames == "" {
			return fmt.Errorf("at least one name in '--names' required, none given")
		}

		// Parse template.
		flagNames, err := templateString(flagNames)
		if err != nil {
			return fmt.Errorf("could not parse names: %w", err)
		}
		names := strings.Split(flagNames, ",")
		for k, v := range names {
			names[k] = strings.TrimSpace(v)
		}

		timeout := vip.GetDuration("timeout")
		interval := vip.GetDuration("interval")
		maxInterval := vip.GetDuration("max-interval")
		retry := vip.GetInt("retry")

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		logrus.Infof("waiting for %s to resolve in DNS ...", strings.Join(names, ", "))
		type result struct {
			name  string
			addrs []string
		}
		results := make(chan result) // DNS results are delivered over here.
		var wg sync.WaitGroup

		start := time.Now()

		for _, name := range names {
			wg.Add(1)
			go func(name string) {
				defer wg.Done()
				var bo backoff.BackOff
				b := backoff.NewExponentialBackOff()
				b.InitialInterval = interval
				b.MaxInterval = maxInterval
				bo = b
				if retry > 0 {
					bo = backoff.WithMaxRetries(b, uint64(retry))
				}
				boctx := backoff.WithContext(bo, ctx)
				op := func() error {
					addrs, err := net.LookupHost(name)
					if err != nil {
						fmt.Println(err)
						return err
					}
					results <- result{
						name:  name,
						addrs: addrs,
					}
					return nil
				}
				if err := backoff.RetryNotify(op, boctx, func(err error, d time.Duration) {
					logrus.Debugf("retrying DNS resolution of %s in %s", name, d)
				}); err != nil {
					logrus.Debugf("error during lookup of %s: %v", name, err)
				}
				logrus.Debugf("resolution done for %s after %s", name, time.Since(start))
			}(name)
		}

		go func() {
			wg.Wait()
			close(results)
		}()

		select {
		case result := <-results:
			cancel()
			if len(result.addrs) > 0 {
				logrus.Infof("got first results %s for %s", strings.Join(result.addrs, ", "), result.name)
				return nil
			}
			logrus.Errorf("could not resolve any of these names in time: %s", strings.Join(names, ", "))
			os.Exit(1)
		case err := <-ctx.Done():
			logrus.Error(err)
			os.Exit(1)
		}

		return nil
	},
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["waitfor"] = v

	flags := waitForCmd.Flags()

	flags.CountP("verbosity", "v", "Verbosity level (--v: verbose, --vv: debug, --vvv: trace)")
	flags.String("names", "", "Hostnames to resolve. Use go template language here if required.")

	flags.IntP("retry", "r", -1, "Retry this many times before giving up. Negative values mean 'infinite'.")
	flags.DurationP("interval", "i", time.Millisecond*500, "Starting retry interval between failed attempts.")
	flags.DurationP("max-interval", "m", time.Second*10, "Max retry interval between failed attempts.")
	flags.DurationP("timeout", "t", time.Minute*5, "Timeout for the whole operation (including retries).")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(waitForCmd)
}
