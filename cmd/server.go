package cmd

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"mgnlboot/cmd/health"
	"net/http"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

type server struct {
	Shutdown context.CancelFunc
	Done     chan error // Closed when we're done.

	ctx            context.Context
	address        string // Where we listen on.
	srv            *http.Server
	r              *mux.Router
	readinessCheck *health.Check   // The current status as by the readiness check.
	livenessCheck  *health.Check   // Check if the Magnolia instance is live.
	al             *ActivationLock // The lock collection holding activation locks.
}

func newServer(ctx context.Context, address string, livez, readyz *health.Check) *server {
	ctx, cancel := context.WithCancel(ctx)
	s := &server{
		Shutdown:       cancel,
		Done:           make(chan error, 2),
		ctx:            ctx,
		address:        address,
		readinessCheck: readyz,
		livenessCheck:  livez,
		al:             NewActivationLock(),
	}

	r := mux.NewRouter()
	r.HandleFunc("/healthz", s.handleHealthz()).Methods(http.MethodGet)
	r.HandleFunc("/readyz", s.handleReadyz()).Methods(http.MethodGet)
	r.HandleFunc("/livez", s.handleLivez()).Methods(http.MethodGet)

	logw := logrus.StandardLogger().WriterLevel(logrus.DebugLevel) // Don't clutter logs.
	lr := handlers.LoggingHandler(logw, r)

	srv := &http.Server{
		Handler:      lr,
		Addr:         address,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
	}
	s.srv = srv
	s.r = r

	return s
}

// Run the server asynchronously.
func (s *server) Run() {
	go func() {
		defer close(s.Done)
		s.Done <- s.srv.ListenAndServe()
	}()

	go func() {
		defer logrus.Infof("shutting down server ...")
		<-s.ctx.Done()
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		if err := s.srv.Shutdown(ctx); err != nil {
			logrus.Errorf("error while shutting down: %s", err)
		}

	}()

	go s.readinessCheck.Run(s.ctx) // Starts embedded liveness check too.
}

type resulter interface {
	Result() health.Result
}

func statusHandler(check resulter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var buf bytes.Buffer
		msg := check.Result()
		if err := json.NewEncoder(&buf).Encode(msg); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			logrus.Error(err)
			return
		}
		w.Header().Set("Content-Type", health.ContentType)
		if msg.Status != health.StatusPass {
			w.WriteHeader(http.StatusServiceUnavailable)
		}
		if _, err := io.Copy(w, &buf); err != nil {
			logrus.Errorf("error sending error to client: %s", err)
		}
	}
}

// Deprecated. Here for backwards compatibility. Use /readyz and /livez instead.
func (s *server) handleHealthz() http.HandlerFunc {
	return statusHandler(s.readinessCheck)
}

func (s *server) handleLivez() http.HandlerFunc {
	return statusHandler(s.livenessCheck)
}

func (s *server) handleReadyz() http.HandlerFunc {
	return statusHandler(s.readinessCheck)
}
