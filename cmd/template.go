package cmd

import (
	"bufio"
	"bytes"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"reflect"
	"regexp"
	"strings"
	"text/template"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

// getEnvMap returns the environment as a map of strings.
func getEnvMap() map[string]string {
	m := make(map[string]string)
	for _, v := range os.Environ() {
		kv := strings.SplitN(v, "=", 2)
		m[kv[0]] = kv[1]
	}
	return m
}

func readProperties(in io.Reader) (map[string]string, error) {
	props := make(map[string]string)
	scanner := bufio.NewScanner(in)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "#") {
			continue
		}
		ll := strings.Split(line, "=")
		if len(ll) != 2 {
			continue
		}
		k := strings.TrimSpace(ll[0])
		v := strings.TrimSpace(ll[1])
		props[k] = v
	}
	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("error reading from input: %w", err)
	}
	return props, nil
}

// This extracts the public key out of an activation key secret.
func extractPubKey(in string) (string, error) {
	props, err := readProperties(strings.NewReader(in))
	if err != nil {
		return "", fmt.Errorf("could not extract public key: %w", err)
	}
	if v, ok := props["key.public"]; ok {
		return v, nil
	}
	return "", fmt.Errorf("no public key found in %s", in)
}

// pkcs1PubKey returns the PEM-formatted public key (PKCS1) from a PEM-formatted
// private key (PKCS8).
func pkcs1PubKey(private string) (string, error) {
	block, _ := pem.Decode([]byte(private)) // We don't care about the rest.
	if block == nil {
		return "", fmt.Errorf("could not parse PKCS1/8 private key")
	}
	var (
		priv interface{}
		err  error
	)
	switch block.Type {
	case "PRIVATE KEY":
		priv, err = x509.ParsePKCS8PrivateKey(block.Bytes)
	case "RSA PRIVATE KEY":
		priv, err = x509.ParsePKCS1PrivateKey(block.Bytes)
	}
	if err != nil {
		return "", fmt.Errorf("could not parse private key: %w", err)
	}
	privKey, ok := priv.(*rsa.PrivateKey)
	if !ok {
		return "", fmt.Errorf("other key than RSA currently not supported")
	}
	pubBytes, err := x509.MarshalPKIXPublicKey(&privKey.PublicKey)
	if err != nil {
		return "", err
	}
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: pubBytes,
	}
	var out bytes.Buffer
	if err := pem.Encode(&out, block); err != nil {
		return "", fmt.Errorf("error encoding public key pem blocK: %w", err)
	}
	return out.String(), nil
}

// podOrdinal returns the ordinal number of this pod. Useful for statefulSets
// when you want to 1:1 match pods based on their ordinal number e.g.
//
//  app-pod-0	-> db-pod-0
//  app-pod-1	-> db-pod-1
//  app-pod-2	-> db-pod-2
//  ...
func podOrdinal(hostname string) (string, error) {
	const rePodOrdinal = `-[\d]+$`
	hostname = strings.TrimSpace(hostname)
	re := regexp.MustCompile(rePodOrdinal)
	sub := re.FindString(hostname)
	if sub == "" {
		return "", fmt.Errorf("no ordinal number found in hostname: %s", hostname)
	}
	return strings.TrimPrefix(sub, "-"), nil
}

// jwtToken generates a JWT (base64) with the json data given as payload. It
// signs it with the RSA key given in PEM format. Can be used as a bearer token.
func jwtToken(payload, privKey []byte) (string, error) {
	key, err := jwt.ParseRSAPrivateKeyFromPEM(privKey)
	if err != nil {
		return "", err
	}

	// Map the claims from the payload.
	claims := new(jwt.MapClaims)
	if err := json.Unmarshal(payload, &claims); err != nil {
		return "", fmt.Errorf("error unmarshaling jwt claims: %w", err)
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	signed, err := token.SignedString(key)
	if err != nil {
		return "", err
	}

	return signed, nil
}

func jwtTokenStrings(payload, privkey string) (string, error) {
	return jwtToken([]byte(payload), []byte(privkey))
}

func templateThis(out io.Writer, in io.Reader) error {
	var tpl strings.Builder
	if _, err := io.Copy(&tpl, in); err != nil {
		return fmt.Errorf("could not read template: %w", err)
	}

	template := template.New("instructions")
	template.Funcs(map[string]interface{}{
		"bcrypt": func(password string) (string, error) {
			hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
			if err != nil {
				return "", fmt.Errorf("could not scrypt password: %w", err)
			}
			return string(hash), nil
		},
		"quote": func(in string) string {
			return fmt.Sprintf(`"%s"`, in)
		},
		"pubkey":      extractPubKey,
		"pkcs1PubKey": pkcs1PubKey,
		"genjwt":      jwtTokenStrings,
		"podordinal":  podOrdinal,
		"default": func(def interface{}, value interface{}) interface{} {
			if value == nil {
				return def
			}
			v := reflect.ValueOf(value)
			switch v.Kind() {
			case reflect.String, reflect.Slice, reflect.Array, reflect.Map:
				if v.Len() == 0 {
					return def
				}
			case reflect.Bool:
				if !v.Bool() {
					return def
				}
			default:
				return value
			}
			return value
		},
	})
	var err error
	template, err = template.Parse(tpl.String())
	if err != nil {
		return fmt.Errorf("error parsing template: %w", err)
	}

	data := struct {
		Env  map[string]string
		File fileAccess
	}{
		Env: getEnvMap(),
	}

	//var buf bytes.Buffer
	if err := template.Execute(out, data); err != nil {
		return fmt.Errorf("could not execute template: %w", err)
	}

	return nil
}

// fileAccess helps reading files inside templates.
type fileAccess struct{}

func (f fileAccess) Get(filename string) (string, error) {
	out, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return string(out), nil
}
