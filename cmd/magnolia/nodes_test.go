package magnolia

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"testing"
)

func fixture(path string, t *testing.T) string {
	b, err := ioutil.ReadFile("testdata/fixtures/" + path)
	if err != nil {
		t.Errorf("could not load test data from %s: %v", path, err)
		return ""
	}

	return string(b)
}

func TestNode_Walk(t *testing.T) {
	buf := bytes.NewBufferString(fixture("nodeTree.json", t))

	var tree Node
	if err := json.NewDecoder(buf).Decode(&tree); err != nil {
		t.Fatal(err)
	}

	want := "51ae3379-67cf-4994-9e05-f97cb8bc3e4a"

	type args struct {
		f WalkFunc
	}
	tests := []struct {
		name    string
		node    *Node
		args    args
		wantErr bool
	}{
		{
			name: "simple walk through the park",
			node: &tree,
			args: args{
				f: func(n *Node) error {
					if n.Identifier == want {
						t.Logf("found node: %+v, want = %s", n, want)
					}
					return nil
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.node.Walk(tt.args.f); (err != nil) != tt.wantErr {
				t.Errorf("Node.Walk() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
