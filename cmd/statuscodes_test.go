package cmd

import (
	"encoding/json"
	"reflect"
	"testing"
)

func Test_parseCodeRanges(t *testing.T) {
	tests := []struct {
		name    string
		args    []string
		want    intRangeList
		wantErr bool
	}{
		{
			name: "simple range",
			args: []string{
				"100-299",
			},
			want: intRangeList{
				intRange{100, 299},
			},
			wantErr: false,
		},
		{
			name: "more complex range",
			args: []string{
				"100-299",
				"200",
				"401-499",
				"500",
				"456",
			},
			want: intRangeList{
				intRange{100, 299},
				intRange{200, 200},
				intRange{401, 499},
				intRange{500, 500},
				intRange{456, 456},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseCodeRanges(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseCodeRanges() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parseCodeRanges() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_intRangeList_Compact(t *testing.T) {
	tests := []struct {
		name string
		a    intRangeList
		want intRangeList
	}{
		{
			name: "zero items",
			a:    intRangeList{},
			want: intRangeList{},
		},
		{
			name: "single item",
			a: intRangeList{
				intRange{100, 199},
			},
			want: intRangeList{
				intRange{100, 199},
			},
		},
		{
			name: "non-contiguous",
			a: intRangeList{
				intRange{100, 199},
				intRange{201, 300},
			},
			want: intRangeList{
				intRange{100, 199},
				intRange{201, 300},
			},
		},
		{
			name: "contiguous",
			a: intRangeList{
				intRange{100, 199},
				intRange{200, 300},
			},
			want: intRangeList{
				intRange{100, 300},
			},
		},
		{
			name: "partially contiguous",
			a: intRangeList{
				intRange{100, 150},
				intRange{160, 275},
				intRange{250, 300},
			},
			want: intRangeList{
				intRange{100, 150},
				intRange{160, 300},
			},
		},
		{
			name: "unsorted, partially contiguous",
			a: intRangeList{
				intRange{100, 150},
				intRange{250, 300},
				intRange{160, 275},
			},
			want: intRangeList{
				intRange{100, 150},
				intRange{160, 300},
			},
		},
		{
			name: "unsorted, partially contiguous, more complex",
			a: intRangeList{
				intRange{100, 299},
				intRange{200, 200},
				intRange{401, 499},
				intRange{500, 500},
				intRange{456, 456},
			},
			want: intRangeList{
				intRange{100, 299},
				intRange{401, 500},
			},
		},
		{
			name: "unsorted, partially contiguous, more complex, last one encasing",
			a: intRangeList{
				intRange{100, 299},
				intRange{200, 200},
				intRange{401, 499},
				intRange{500, 500},
				intRange{456, 456},
				intRange{100, 1000},
			},
			want: intRangeList{
				intRange{100, 1000},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.a.Compact()
			if !reflect.DeepEqual(tt.want, got) {
				t.Errorf("Merge() = %v, want = %v", got, tt.want)
			}
		})
	}
}

func Test_intRangeList_UnmarshalJSON(t *testing.T) {
	bb, err := json.Marshal([]string{"200-203", "300-399"})
	if err != nil {
		t.Fatal(err)
	}

	type args struct {
		b []byte
	}
	tests := []struct {
		name    string
		a       *intRangeList
		args    args
		wantErr bool
	}{
		{
			name:    "parse",
			a:       &intRangeList{},
			args:    args{bb},
			wantErr: false,
		},
		{
			name:    "error parsing",
			a:       &intRangeList{},
			args:    args{[]byte(`["200-203", `)},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.a.UnmarshalJSON(tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("intRangeList.UnmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_intRangeList_MarshalJSON(t *testing.T) {
	tests := []struct {
		name    string
		a       intRangeList
		want    []byte
		wantErr bool
	}{
		{
			name: "marshal",
			a: intRangeList{
				intRange{200, 209},
				intRange{300, 399},
			},
			want:    []byte(`["200-209","300-399"]`),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.a.MarshalJSON()
			if (err != nil) != tt.wantErr {
				t.Errorf("intRangeList.MarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("intRangeList.MarshalJSON() = %v, want %v", string(got), string(tt.want))
			}
		})
	}
}
